import type { SupplyData, SupplyDataTo, SupplyType } from "~/utils/supply"

export type ServerSupplyType = SupplyType;
export type ServerSupplyData = SupplyData;
export type ServerSupplyDataTo = SupplyDataTo;
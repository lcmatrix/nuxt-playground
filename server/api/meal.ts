import { useMeals } from "../../composables/useMeals";

export default defineEventHandler((event) => {
  switch (event.node.req.method) {
    case "GET":
      return useMeals();
    default:
      break;
  }
});

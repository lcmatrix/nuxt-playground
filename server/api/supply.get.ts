import { MongoClient } from "mongodb";
import type { ServerSupplyDataTo, ServerSupplyType } from "../utils/supply";

export default defineEventHandler( async (event) => {
    const query = getQuery(event);
    const type = query.type;
    console.log(`Fetch supply for type: ${type}`);
    return await getSupplyData(type?.toString());
});

/**
 * Fetches supply data from database.
 * 
 * @param typeParam supply type
 * @returns array of supply data
 */
async function getSupplyData(typeParam?: string): Promise<ServerSupplyDataTo[]> {
  const url = process.env.MONGO_DB_URL;
  if (!url) {
    console.log("Mongo DB URL is undefined");
    return new Promise((_, reject) => {
      reject("Mongo DB URL is undefined")
    });
  }
  console.log(`Fetching data for ${typeParam as ServerSupplyType}`)
  const client = new MongoClient(url);
  const supplyData: ServerSupplyDataTo[] = [];
  try {
      const db = client.db("haus");
      const collection = db.collection<SupplyDocument>("versorgung");
      const cursor = collection.find<SupplyDocument>({art: typeParam as ServerSupplyType}, {sort: {datum: -1}});
      for await (const doc of cursor) {
          supplyData.push({
            type: doc.art,
            unit: doc.einheit,
            value: doc.stand,
            date: doc.datum,
          });
      }
      cursor.close();
      return new Promise((resolve) => resolve(supplyData));
    } catch (e) {
      console.log(`Error fetching data ${e}`);
      return new Promise((resolve) => resolve([]));
    } finally {
      client.close();
    }
}

/**
 * Model of a supply document.
 */
interface SupplyDocument {
  art: ServerSupplyType,
  stand: number,
  einheit: string,
  datum: string
}

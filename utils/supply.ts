export enum SupplyType {
    Gas = "GAS",
    Strom = "STROM",
    Wasser = "WASSER"
}

export interface SupplyData {
    type: SupplyType,
    unit: string,
    date: Date,
    value: number | string
}

export interface SupplyDataTo {
  type: SupplyType,
  unit: string,
  value: number | string,
  date: string
}

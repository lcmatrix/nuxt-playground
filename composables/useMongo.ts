import { MongoClient } from "mongodb"

export const useMongo = () => {
    const client = new MongoClient(import.meta.env.MONGO_DB_URL);
    return client;
}
import { useMongo } from "./useMongo"

export const useMeals = async () => {
  const client = useMongo();
  try {
    const meals: Meal[] = [];
    const db = client.db("mealplan");
    const collection = db.collection<Meal>("meal");
    const cursor = collection.find<Meal>({});
    for await (const doc of cursor) {
      meals.push(doc);
    }
    cursor.close();
    return meals;
  } catch (e) {
    console.log(e);
    return [];
  } finally {
    client.close();
  }
}


export interface Meal {
  _id?: string;
  id?: string;
  name: string;
  description?: string;
  ingredients?: string[];
}
